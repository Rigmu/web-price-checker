/**
 * Web Price Checker API
 * This is an API for web price checker
 *
 * The version of the OpenAPI document: v1
 * Contact: contact@snippets.local
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


export interface Parser { 
    readonly id?: number;
    class_name: string;
    base_url: string;
    path: Parser.PathEnum;
}
export namespace Parser {
    export type PathEnum = '' | 'web_price_checker_app/parsers/ceneo.py' | 'web_price_checker_app/parsers/parser.py';
    export const PathEnum = {
        Empty: '' as PathEnum,
        WebPriceCheckerAppParsersCeneoPy: 'web_price_checker_app/parsers/ceneo.py' as PathEnum,
        WebPriceCheckerAppParsersParserPy: 'web_price_checker_app/parsers/parser.py' as PathEnum
    };
}


