import os

from celery import Celery
from celery.schedules import crontab

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'web_price_checker.settings')

app = Celery('web_price_checker')

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()

@app.on_after_finalize.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(
        crontab(hour=10, minute=1),
        download_prices.s()
    )

    sender.add_periodic_task(
        crontab(hour=22, minute=1),
        download_prices.s()
    )

@app.task()
def download_prices():
    from web_price_checker_app.services.price_service import PriceService

    price_service = PriceService()
    price_service.download_prices()