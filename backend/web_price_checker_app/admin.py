from django.contrib import admin

from .models import Aggregator, PageSnapshot, Price, Product, Store, Url, Parser

admin.site.register(Aggregator)
admin.site.register(PageSnapshot)
admin.site.register(Price)
admin.site.register(Product)
admin.site.register(Store)
admin.site.register(Url)
admin.site.register(Parser)
