from django.apps import AppConfig
from django.db.models.signals import post_save

class WebPriceCheckerAppConfig(AppConfig):
    name = 'web_price_checker_app'

    def ready(self):
        from web_price_checker_app.signals import on_url_insert

        post_save.connect(on_url_insert, sender=f'{self.name}.Url')