from django.forms import ModelForm, Form
from web_price_checker_app.models import Product

class ProductForm(ModelForm):
    class Meta:
        model = Product
        fields = ['name']