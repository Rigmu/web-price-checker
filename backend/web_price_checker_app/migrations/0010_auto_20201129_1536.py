# Generated by Django 3.1.3 on 2020-11-29 15:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web_price_checker_app', '0009_auto_20201129_1530'),
    ]

    operations = [
        migrations.AlterField(
            model_name='parser',
            name='path',
            field=models.FilePathField(match='\\.py$', path='web_price_checker_app/parsers', unique=True),
        ),
    ]
