from django.db import models

class Parser(models.Model):
    class_name = models.CharField(max_length=100, unique=True)
    base_url = models.URLField(max_length=255, unique=True)
    path = models.FilePathField(path='web_price_checker_app/parsers', match="\.py$", unique=True, allow_folders=False, allow_files=True, null=False, blank=False)

    def __str__(self):
        return f'Parser {self.name}'

class Aggregator(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return f'Aggregator {self.name}'

class Store(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return f'Store {self.name}'

class Product(models.Model):
    name = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return f'Product {self.name}'

class Url(models.Model):
    product = models.ForeignKey(Product, on_delete=models.DO_NOTHING)
    url = models.URLField(unique=True)

    def __str__(self):
        return self.url

class PageSnapshot(models.Model):
    url = models.ForeignKey(Url, on_delete=models.DO_NOTHING)
    snapshot = models.TextField()
    datetime = models.DateTimeField()

class Price(models.Model):
    store = models.ForeignKey(Store, related_name="prices", on_delete=models.DO_NOTHING)
    product = models.ForeignKey(Product, on_delete=models.DO_NOTHING)
    url = models.ForeignKey(Url, on_delete=models.DO_NOTHING)
    datetime = models.DateTimeField()
    price = models.FloatField()
