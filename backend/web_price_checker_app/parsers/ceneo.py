from urllib.parse import urlparse
from urllib.parse import urlsplit

from web_price_checker_app.parsers.parser import Parser
from web_price_checker_app.domain.instantaneous_price import InstantaneousPrice

class Ceneo(Parser):
    def __init__(self, page_source):
        super(Ceneo, self).__init__(page_source)

        self.page_source = page_source

    def get_parser_name(self):
        return 'ceneo'

    def get_parser_class_name(self):
        return 'Ceneo'

    def get_offer_container_elements(self):
        return self.page.select('.product-offer-2020__container')

    def get_offer_store_url_attribute(self, element):
        return element.attrs['data-shopurl']

    def get_offer_price_attribute(self, element):
        return element.attrs['data-price']

    def get_offer_price_element(self, element):
        return element.select('.price')

    def get_offer_price(self, element):
        price_element = self.get_offer_price_element(element)
        return price_element[0].get_text()

    def get_store_name_from_element(self, element):
        url = self.get_offer_store_url_attribute(element)
        netloc = urlparse(f'//{url}').netloc
        host = netloc[:netloc.find('.')]

        return host

    def get_element_attributes(self, element):
        return element.attrs

    def get_price(self):
        return self.get_price_element().text

    def get_prices(self):
        price_elements = self.get_price_elements()
        return [price_element[0].get_text() for price_element in price_elements]

    def get_instantaneous_price(self, element):
        return InstantaneousPrice(self.get_store_name_from_element(element), self.get_offer_store_url_attribute(element), self.get_offer_price(element), self.get_offer_price_attribute(element))

    def parse(self):
        offer_container_elements = self.get_offer_container_elements()

        return [self.get_instantaneous_price(element) for element in offer_container_elements]