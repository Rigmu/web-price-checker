from bs4 import BeautifulSoup
from typing import List

from web_price_checker_app.domain.instantaneous_price import InstantaneousPrice

class Parser:
    def __init__(self, page_source):
        self.page = BeautifulSoup(page_source, 'html.parser')

    def get_parser_name(self):
        pass

    def get_price_element(self):
        pass

    def get_price_elements(self):
        pass

    def get_price(self):
        pass

    def get_prices(self):
        pass

    def parse(self) -> List[InstantaneousPrice]:
        pass
