from django.contrib.auth.models import User, Group
from web_price_checker_app.models import Product, Aggregator, PageSnapshot, Parser, Price, Store, Url
from rest_framework import serializers


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups']


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']

class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ['id', 'name']

class AggregatorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Aggregator
        fields = ['id', 'name']

class PageSnapshotSerializer(serializers.ModelSerializer):
    class Meta:
        model = PageSnapshot
        fields = ['id', 'url', 'snapshot', 'datetime']

class ParserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Parser
        fields = ['id', 'class_name', 'base_url', 'path']

class PriceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Price
        fields = ['id', 'store', 'product', 'url', 'datetime', 'price']

class UrlSerializer(serializers.ModelSerializer):
    # product = ProductSerializer(many=False, read_only=False)
    class Meta:
        model = Url
        fields = ['id', 'product', 'url']

class StoreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Store
        fields = ['id', 'name', 'prices']

