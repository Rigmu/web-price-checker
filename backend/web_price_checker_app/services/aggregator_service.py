from web_price_checker_app.models import Aggregator, Url

class AggregatorService:
    def __init__(self):
        pass

    def get_aggregator_from_url(self, url: Url) -> Aggregator:
        aggregators = Aggregator.objects.all()

        for aggregator in aggregators:
            if aggregator.name.lower() in url.url:
                return aggregator

        aggregator_names = [aggregator.name for aggregator in aggregators]

        raise Exception(f'Aggregator name not defined in {aggregator_names} for url {url.url}')