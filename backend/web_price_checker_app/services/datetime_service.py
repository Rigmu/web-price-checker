import datetime
import pytz

class DateTimeService:
    def __init__(self):
        pass

    @staticmethod
    def get_current_UTC_datetime():
        return pytz.utc.localize(datetime.datetime.now())