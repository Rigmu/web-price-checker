import os

class DottedPathService:
    def __init__(self):
        pass

    @staticmethod
    def path_to_dotted_path(path_name_extension: str) -> str:
        path_name = os.path.splitext(path_name_extension)[0]
        dotted_path = path_name.replace('/', '.')

        return dotted_path

    @staticmethod
    def add(dotted_path: str, new_part: str) -> str:
        return '.'.join([dotted_path, new_part])