from typing import List

from web_price_checker_app.models import PageSnapshot, Url

from web_price_checker_app.services.datetime_service import DateTimeService

class PageSnapshotService:
    def __init__(self):
        pass

    def get_latest_page_snapshot(self) -> PageSnapshot:
        # we are assuming that adding a URL will take a page snapshot
        # meaning there will always be at least one
        return PageSnapshot.objects.order_by('datetime').first()

    def get_latest_page_snapshot_for_url(self, url) -> PageSnapshot:
        return PageSnapshot.objects.filter(url=url).order_by('datetime').first()

    def get_page_snapshots_for_url(self, url: Url) -> List[PageSnapshot]:
        return PageSnapshot.objects.filter(url=url)

    def exists_for_url(self, url: Url) -> bool:
        return PageSnapshot.objects.filter(url=url).exists()

    def insert_page_snapshot(self, url: Url, page_source: str):
        page_snapshot = PageSnapshot(url=url, snapshot=page_source, datetime=DateTimeService.get_current_UTC_datetime())
        page_snapshot.save()