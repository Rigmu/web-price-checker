from pydoc import locate

from web_price_checker_app.models import Url, Parser
from typing import List

from web_price_checker_app.services.dotted_path_service import DottedPathService
from web_price_checker_app.services.url_service import UrlService

from web_price_checker_app.parsers.parser import Parser as HtmlParser

class ParserService:
    def __init__(self):
        self.url_service = UrlService()
        pass

    def get_parser_from_url(self, url: Url) -> Parser:
        parsers = Parser.objects.all()
        found_parser = [parser for parser in parsers if parser.base_url in url.url]

        if found_parser:
            return found_parser[0]

    def get_parser_instance_from_url(self, url: Url, page_source: str) -> HtmlParser:
        parser = self.get_parser_from_url(url)
        parser_type = self.get_parser_type_for_parser(parser)
        return parser_type(page_source)

    def get_parser_types_for_parsers(self, parsers: List[Parser]) -> list:
        return [locate(parser.path) for parser in parsers]

    def get_parser_type_for_parser(self, parser: Parser) -> type:
        dotted_path = DottedPathService.path_to_dotted_path(parser.path)
        dotted_path_class = DottedPathService.add(dotted_path, parser.class_name)

        return locate(dotted_path_class)

    def get_parser_instance_for_parser_info(self, parser: Parser, page_source: str) -> HtmlParser:
        parser_type = self.get_parser_type_for_parser(parser)
        return parser_type(page_source)