import datetime

from web_price_checker_app.models import Store, Url, PageSnapshot, Price

from web_price_checker_app.services.web_service import WebService
from web_price_checker_app.services.page_snapshot_service import PageSnapshotService
from web_price_checker_app.services.aggregator_service import AggregatorService
from web_price_checker_app.services.store_service import StoreService
from web_price_checker_app.services.parser_service import ParserService
from web_price_checker_app.services.datetime_service import DateTimeService

class PriceService:
    def __init__(self):
        self.web_service = WebService()
        self.page_snapshot_service = PageSnapshotService()
        self.store_service = StoreService()
        self.aggregator_service = AggregatorService()
        self.parser_service = ParserService()

    def insert_page_snapshots_for_new_urls(self):
        urls = Url.objects.all()

        for url in urls:
            if self.page_snapshot_service.exists_for_url(url):
                continue

            page_source = self.web_service.get_html_source_for_url(url)
            self.page_snapshot_service.insert_page_snapshot(url, page_source)

    def download_prices(self):
        urls = Url.objects.all()

        for url in urls:
            latest_page_snapshot = self.page_snapshot_service.get_latest_page_snapshot_for_url(url)

            latest_datetime = latest_page_snapshot.datetime
            future_datetime = latest_datetime + datetime.timedelta(days=1)

            current_UTC_datetime = DateTimeService.get_current_UTC_datetime()

            page_source = latest_page_snapshot.snapshot

            if future_datetime < current_UTC_datetime:
                page_source = self.web_service.get_html_source_for_url(url)
                self.page_snapshot_service.insert_page_snapshot(url, page_source)

            parser = self.parser_service.get_parser_instance_from_url(url, page_source)
            instantenous_prices = parser.parse()

            for instantenous_price in instantenous_prices:
                price_value = float(instantenous_price.accurate_price.replace(',', '.'))
                store, created = Store.objects.get_or_create(name=instantenous_price.store_name)
                price = Price(store=store, product=url.product, url=url, datetime=current_UTC_datetime, price=price_value)
                price.save()
