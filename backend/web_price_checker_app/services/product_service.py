from typing import List

from web_price_checker_app.models import Product

class ProductService:
    def __init__(self):
        pass

    def insert_product(self, name: str) -> Product:
        new_product = Product(name=name)
        new_product.save()

        return new_product

    def get_products(self) -> List[Product]:
        return Product.objects.all()