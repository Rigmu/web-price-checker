from web_price_checker_app.models import Store, Url

class StoreService:
    def __init__(self):
        pass

    def get_store_from_url(self, url: Url) -> Store:
        stores = Store.objects.all()

        for store in stores:
            if store.name.lower() in url.url:
                return store

        store_names = [store.name for store in stores]

        raise Exception(f'Store name not defined in {store_names} for url {url}')