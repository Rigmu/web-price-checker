from urllib.parse import urlparse

from web_price_checker_app.models import Store, Product, Url, PageSnapshot

from web_price_checker_app.services.web_service import WebService
from web_price_checker_app.services.page_snapshot_service import PageSnapshotService

class UrlService:
    def __init__(self):
        self.web_service = WebService()
        self.page_snapshot_service = PageSnapshotService()
        pass

    def insert_url(self, store: Store, product: Product, url: str) -> bool:
        entry = Url.objects.get(url=url)
        if Url.objects.filter(pk=entry.pk).exists():
            return False

        new_url = Url(store, product, url)
        page_source = self.web_service.get_html_source_for_url(new_url)

        new_url.save()
        self.page_snapshot_service.insert_page_snapshot(new_url, page_source)

        return True

    def get_netloc_from_url(self, url:str) -> str:
        return urlparse(f'//{url}').netloc

    def get_host_from_url(self, url: str) -> str:
        netloc = self.get_netloc_from_url(url)
        host = netloc[:netloc.find('.')]

        return host