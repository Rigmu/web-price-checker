import requests

from web_price_checker_app.models import Url

class WebService:
    def __init__(self):
        pass

    def get_html_source_for_url(self, url: Url) -> str:
        session = requests.Session()
        response = session.get(url.url)

        return response.text