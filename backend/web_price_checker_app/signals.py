from django.db.models.signals import post_save
from django.dispatch import receiver
from web_price_checker_app.models import Url
from web_price_checker_app.services.page_snapshot_service import PageSnapshotService
from web_price_checker_app.services.web_service import WebService

@receiver(post_save, sender=Url)
def on_url_insert(sender, **kwargs):
    url = kwargs['instance']

    web_service = WebService()
    page_source = web_service.get_html_source_for_url(url)

    page_snapshot_service = PageSnapshotService()
    page_snapshot_service.insert_page_snapshot(url, page_source)

    pass