from django.urls import path

from . import views

urlpatterns = [
    path('index', views.index, name='index'),
    path('product', views.product, name='product'),
    path('product/add', views.create_product, name='product_add')
]
