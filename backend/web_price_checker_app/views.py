from django.contrib.auth.models import User, Group
from web_price_checker_app.models import Product, Aggregator, PageSnapshot, Parser, Price, Store, Url
from rest_framework import viewsets
from rest_framework import permissions
from web_price_checker_app.serializers import UserSerializer, GroupSerializer, ProductSerializer, AggregatorSerializer, PageSnapshotSerializer, ParserSerializer, PriceSerializer, StoreSerializer, UrlSerializer


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    # permission_classes = [permissions.IsAuthenticated]


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    # permission_classes = [permissions.IsAuthenticated]

class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    permission_classes = [permissions.AllowAny]

class AggregatorViewSet(viewsets.ModelViewSet):
    queryset = Aggregator.objects.all()
    serializer_class = AggregatorSerializer
    permission_classes = [permissions.AllowAny]

class PageSnapshotViewSet(viewsets.ModelViewSet):
    queryset = PageSnapshot.objects.all()
    serializer_class = PageSnapshotSerializer
    permission_classes = [permissions.AllowAny]

class ParserViewSet(viewsets.ModelViewSet):
    queryset = Parser.objects.all()
    serializer_class = ParserSerializer
    permission_classes = [permissions.AllowAny]

class PriceViewSet(viewsets.ModelViewSet):
    queryset = Price.objects.all()
    serializer_class = PriceSerializer
    permission_classes = [permissions.AllowAny]

class StoreViewSet(viewsets.ModelViewSet):
    queryset = Store.objects.all()
    serializer_class = StoreSerializer
    permission_classes = [permissions.AllowAny]

class UrlViewSet(viewsets.ModelViewSet):
    queryset = Url.objects.all()
    serializer_class = UrlSerializer
    permission_classes = [permissions.AllowAny]
