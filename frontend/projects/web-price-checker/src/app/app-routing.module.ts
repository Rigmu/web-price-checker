import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ParserAddComponent } from './components/parser-add/parser-add.component';
import { ProductDashboardComponent } from './components/product-dashboard/product-dashboard.component';
import { StoreAddComponent } from './components/store-add/store-add.component';
import { UrlAddComponent } from './components/url-add/url-add.component';
import { UrlListComponent } from './components/url-list/url-list.component';

const routes: Routes = [
  { path: 'product-dashboard', component: ProductDashboardComponent },
  { path: 'url-list', component: UrlListComponent },
  { path: 'url-add', component: UrlAddComponent },
  { path: 'store-add', component: StoreAddComponent },
  { path: 'parser-add', component: ParserAddComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
