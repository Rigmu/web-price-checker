import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AngularMaterialModule } from './material.module'

import {
  ApiModule,
  Configuration,
  ConfigurationParameters,
} from 'wdftadtiowrhstohbaazkosfxorpxmdk';
import { HttpClientModule } from '@angular/common/http';
import { ProductAddComponent } from './components/product-add/product-add.component';
import { ProductDashboardComponent } from './components/product-dashboard/product-dashboard.component';
import { ProductListComponent } from './components/product-list/product-list.component';
import { UrlListComponent } from './components/url-list/url-list.component';
import { UrlAddComponent } from './components/url-add/url-add.component';
import { StoreAddComponent } from './components/store-add/store-add.component';
import { ParserAddComponent } from './components/parser-add/parser-add.component';

export function apiConfigFactory(): Configuration {
  const params: ConfigurationParameters = {
    // set configuration parameters here.
  };
  return new Configuration(params);
}

@NgModule({
  imports: [
    ApiModule.forRoot(apiConfigFactory),
    HttpClientModule,
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularMaterialModule
  ],
  declarations: [AppComponent, ProductAddComponent, ProductDashboardComponent, ProductListComponent, UrlListComponent, UrlAddComponent, StoreAddComponent, ParserAddComponent],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
