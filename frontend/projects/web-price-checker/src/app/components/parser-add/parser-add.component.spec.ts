import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ParserAddComponent } from './parser-add.component';

describe('ParserAddComponent', () => {
  let component: ParserAddComponent;
  let fixture: ComponentFixture<ParserAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ParserAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ParserAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
