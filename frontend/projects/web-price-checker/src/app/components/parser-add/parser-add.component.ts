import { Component, OnInit } from '@angular/core';
import { Parser, ParsersService } from 'wdftadtiowrhstohbaazkosfxorpxmdk';

@Component({
  selector: 'app-parser-add',
  templateUrl: './parser-add.component.html',
  styleUrls: ['./parser-add.component.css']
})
export class ParserAddComponent implements OnInit {
  readonly possiblePaths: Parser.PathEnum[] = Object.values(Parser.PathEnum);
  parser: Parser = this.resetParser();

  constructor(private parsersService: ParsersService) { 
  }

  ngOnInit(): void {
  }

  resetParser(): Parser {
    return { base_url: "", class_name: "", path: "" };
  }

  addParser() {
    this.parsersService.parsersCreate(this.parser).subscribe(_ => this.parser = this.resetParser());
  }
}
