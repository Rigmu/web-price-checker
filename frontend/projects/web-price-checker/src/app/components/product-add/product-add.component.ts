import { Component, OnInit } from '@angular/core';
import { Product, ProductsService, Url } from 'wdftadtiowrhstohbaazkosfxorpxmdk';
import { ProductCommunicationService } from '../../services/product-communication.service';

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.css']
})
export class ProductAddComponent implements OnInit {
  product: Product = this.resetProduct();

  constructor(private productService: ProductsService, private productCommunicationService: ProductCommunicationService) { }

  ngOnInit(): void {
  }

  addProduct(): void {
    this.productService.productsCreate(this.product).subscribe(_ => this.product = this.resetProduct());
    this.productCommunicationService.announceProductAdd(this.product);
  }

  resetProduct(): Product {
    return { name: "" };
  }
}
