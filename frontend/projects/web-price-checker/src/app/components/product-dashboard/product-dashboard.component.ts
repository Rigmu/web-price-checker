import { Component, OnInit } from '@angular/core';
import { Product } from 'wdftadtiowrhstohbaazkosfxorpxmdk';
import { ProductCommunicationService } from '../../services/product-communication.service';

@Component({
  selector: 'app-product-dashboard',
  templateUrl: './product-dashboard.component.html',
  styleUrls: ['./product-dashboard.component.css'],
  providers: [ProductCommunicationService]
})
export class ProductDashboardComponent implements OnInit {

  constructor(private productCommunicationService: ProductCommunicationService) {
  }

  ngOnInit(): void {
  }

  announceProductAdd(product: Product) {
    this.productCommunicationService.announceProductAdd(product);
  }
}
