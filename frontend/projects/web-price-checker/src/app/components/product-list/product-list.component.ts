import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit } from '@angular/core';
import { Product, ProductsService } from 'wdftadtiowrhstohbaazkosfxorpxmdk';
import { ProductCommunicationService } from '../../services/product-communication.service';
import { of } from 'rxjs';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  selection = new SelectionModel<Product>(true, []);

  products: Product[];
  productCount: number;

  displayedColumns: string[] = ['select', 'id', 'name'];

  constructor(private productService: ProductsService, private productCommunicationService: ProductCommunicationService) {
    productCommunicationService.productAdded.subscribe(_ => this.getProducts());
  }

  ngOnInit(): void {
    this.getProducts();
  }

  getProducts(): void {
    this.productService
      .productsList()
      .subscribe((data) => (this.products = data));
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.products.length;
    return numSelected == numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.products.forEach(row => this.selection.select(row));
  }

  removeSelectedProducts() {
    const selectedProdcuts = of(...this.selection.selected);
    selectedProdcuts.subscribe(product => this.productService.productsDelete(product.id).subscribe(_ => this.getProducts()));
  }
}
