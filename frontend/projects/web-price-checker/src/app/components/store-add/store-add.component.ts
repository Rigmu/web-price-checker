import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Parser, ParsersService, Store, StoresService } from 'wdftadtiowrhstohbaazkosfxorpxmdk';

@Component({
  selector: 'app-store-add',
  templateUrl: './store-add.component.html',
  styleUrls: ['./store-add.component.css']
})
export class StoreAddComponent implements OnInit {
  parsers$: Observable<Parser[]>;

  store: Store = this.resetStore();

  constructor(private storesService: StoresService, private parsersService: ParsersService) { 

  }

  ngOnInit(): void {
    this.parsers$ = this.parsersService.parsersList();
  }

  resetStore(): Store {
    return { name: "", prices: [] };
  }

  addStore(): void {
    this.storesService.storesCreate(this.store).subscribe(_ => this.store = this.resetStore());
  }
}
