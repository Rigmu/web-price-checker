import { Component, OnInit } from '@angular/core';
import { Url, Store, StoresService, ProductsService, Product, UrlsService } from 'wdftadtiowrhstohbaazkosfxorpxmdk';
import { flatMap, tap } from 'rxjs/operators';
import { BehaviorSubject, combineLatest, forkJoin, from, Observable, of } from 'rxjs';

@Component({
  selector: 'app-url-add',
  templateUrl: './url-add.component.html',
  styleUrls: ['./url-add.component.css']
})
export class UrlAddComponent implements OnInit {
  products$: Observable<Product[]>;

  products: Product[];
  url: Url = this.resetUrl();

  constructor(private urlService: UrlsService, private productService: ProductsService) {
  }

  ngOnInit(): void {
    this.products$ = this.getProducts();
  }

  getProducts(): Observable<Product[]> {
    return this.productService.productsList();
  }

  resetUrl(): Url {
    return { url: "", product: -1 };
  }

  getValidatedUrl(url: string): URL {
    try {
      return new URL(url);
    } catch (error) {
      throw error;
    }
  }

  addUrl() {
    const url = this.getValidatedUrl(this.url.url);
    this.url.url = url.href;

    this.urlService.urlsCreate(this.url).subscribe(_ => this.url = this.resetUrl());
  }
}
