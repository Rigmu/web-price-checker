import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit } from '@angular/core';
import { Product, ProductsService, Url, UrlsService } from 'wdftadtiowrhstohbaazkosfxorpxmdk';

interface ProductsPerUrl {
  url: Url;
  products: Product[];
}

@Component({
  selector: 'app-url-list',
  templateUrl: './url-list.component.html',
  styleUrls: ['./url-list.component.css']
})
export class UrlListComponent implements OnInit {
  selection = new SelectionModel<Url>(true, []);

  urls: Url[];
  products: Product[];
  productsPerUrl: ProductsPerUrl[];

  displayedColumns: string[] = ['select', 'id', 'product-name', 'url'];

  constructor(private urlService: UrlsService, private productService: ProductsService) { }

  ngOnInit(): void {
    this.getUrls();
  }

  getUrls(): void {
    this.urlService.urlsList().subscribe((urls) => (this.urls = urls));
  }

  getProducts(): void {
    this.productService.productsList().subscribe(products => this.products = products);
  }

  getProductForUrl(url: Url): Product {
    return this.products.find(product => product.id == url.product);
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.urls.length;
    return numSelected == numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.urls.forEach(row => this.selection.select(row));
  }
}
