import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Product } from 'wdftadtiowrhstohbaazkosfxorpxmdk';

@Injectable()
export class ProductCommunicationService {
  private productAddedSource = new Subject<Product>();

  productAdded = this.productAddedSource.asObservable();

  announceProductAdd(product: Product) {
    this.productAddedSource.next(product);
  }
}
